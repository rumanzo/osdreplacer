Name: osdreplacer
Version:	1.2
Release:	2%{?dist}
Packager:   Alexey Kostin <rumanzo@yandex.ru>
Summary:	Web tool for ceph osd replace
BuildArch:  noarch
Group:		System/Filesystems
License:	GPLv2
URL:		http://go.itsirius.su/akostin/osdreplacer/
Source0:	http://repo.sto.fintech.ru/archive/osdreplacer-%{version}.tar.xz
BuildRequires: rsync python
Requires:   python-netifaces python-flask python-pyudev httpd mod_wsgi python-urllib3 ceph-base selinux-policy

%description
Multi-server web portal for replace changed osd

%prep
%setup -q

%install
mkdir -p %{buildroot}/opt/%{name}/
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
rsync -aAX ./ %{buildroot}/opt/%{name}/ --exclude='apache_osdreplace.conf' --exclude='osdreplacer.pp'
install -m 0644 ./apache_osdreplace.conf %{buildroot}%{_sysconfdir}/httpd/conf.d/osdreplacer.conf
install -d %{buildroot}%{_datadir}/selinux/packages/
install -p -m 644 ./osdreplacer.pp %{buildroot}%{_datadir}/selinux/packages/osdreplacer.pp


%files
%defattr(0644,ceph,ceph,0755)
%dir /opt/%{name}/
%dir /opt/%{name}/templates/
%dir /opt/%{name}/osddb/
%config /opt/%{name}/osdreplace.conf
/opt/%{name}/api.py*
/opt/%{name}/api.wsgi
/opt/%{name}/osddb/__init__.py*
/opt/%{name}/osddb/getpoints.py*
/opt/%{name}/templates/countdown.html
/opt/%{name}/templates/index.html
/opt/%{name}/templates/wait.html
%{_datadir}/selinux/packages/osdreplacer.pp
%attr(0644,root,root) %{_sysconfdir}/httpd/conf.d/osdreplacer.conf

%post
semodule -s targeted -i %{_datadir}/selinux/packages/osdreplacer.pp

%postun
if [ $1 -eq 0 ] ; then
 /usr/sbin/semodule -s targeted -r osdreplacer &> /dev/null || :
fi

%changelog
* Mon Oct 1 2018 Alexey Kostin <rumanzo@yandex.ru> 1.2
- Added selinux module

* Tue Sep 21 2018 Alexey Kostin <rumanzo@yandex.ru> 1.1
- Fix for for NVME disks

* Tue Sep 04 2018 Alexey Kostin <rumanzo@yandex.ru> 1.0
- Initial build
