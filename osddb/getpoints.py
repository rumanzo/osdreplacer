#!/usr/bin/env python2
import os
import platform
import re
import sqlite3
import subprocess

import pyudev


class getpoints(object):
    def __init__(self, host=platform.node(), rootpath=os.getcwd() + os.sep):
        print(rootpath)
        self.rootpath = rootpath
        getpoints.context = pyudev.Context()
        getpoints.mountpoints = self.getmountpoints()
        self.updatedb()
        getpoints.hostname = host

    def getosdid(self, mountpoint):  # Return ceph osd id for mountpoint path
        with open(mountpoint + '/whoami', 'r') as file:
            return file.readline().rstrip('\n')

    def normalize(self, dev):  # dev without partitions
        return dev[:-2] if 'nvme' in dev else dev[:-1]

    def getmountpoints(self):
        stdout, stderr = subprocess.Popen(['mount'], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        return list(filter(lambda x: not re.match(r'^$', x), re.split('\n', stdout.decode('UTF-8'))))

    def getcephmountpoints(self):  # return all osd mountpoints
        cephmountpoints = []
        for mountpoint in self.mountpoints:
            if '/var/lib/ceph/osd' in mountpoint:
                splitted = re.split('\s+', mountpoint)
                device = pyudev.Device.from_device_file(self.context, splitted[0])  # udev part object
                origdevice = device.parent  # udev block device object
                if any([True for part in origdevice.children if part.get('ID_FS_TYPE') == 'linux_raid_member']):
                    continue
                osdid = self.getosdid(splitted[2])
                cephmountpoints.append({'osdid': int(osdid), 'block': origdevice['DEVNAME'],
                                        'serial': (origdevice.get('ID_SCSI_SERIAL')
                                                   if origdevice.get('ID_SCSI_SERIAL')
                                                   else origdevice.get('ID_SERIAL')),
                                        'scsipath': [origdevice.get(x) for x in ['ID_SAS_PATH', 'ID_PATH', 'DEVPATH']
							if origdevice.get(x)][0]})
        return cephmountpoints

    def getdevices(self):
        alldevices = []
        for dev in self.context.list_devices(subsystem='block', DEVTYPE='disk'):
            if not dev.get('DEVNAME').startswith('/dev/md') and not dev.get('ID_CDROM')\
                    and (dev.get('ID_PATH') or dev.get('ID_SAS_PATH') or ('nvme' in dev.get('DEVNAME'))):
                alldevices.append({'block': dev['DEVNAME'],
                                   'serial': (dev.get('ID_SCSI_SERIAL') if dev.get('ID_SCSI_SERIAL') else dev.get(
                                       'ID_SERIAL')),
                                   'scsipath': [dev.get(x) for x in ['ID_SAS_PATH', 'ID_PATH', 'DEVPATH']
							if dev.get(x)][0]})
        return alldevices

    def updatedb(self, updaterow=False, osdforclear=None):
        conn = sqlite3.connect(self.rootpath + 'osd.db')
        c = conn.cursor()
        c.execute('CREATE TABLE IF NOT EXISTS osd (osdid integer, block text, serial text, scsipath text  PRIMARY KEY)')
        conn.commit()
        if updaterow:
            c.execute('DELETE FROM osd where osdid=?', (osdforclear,))
            conn.commit()
        for osd in self.getcephmountpoints():
            c.execute('INSERT OR IGNORE INTO osd (osdid, block, serial, scsipath) values(?,?,?,?)',
                      (osd['osdid'], osd['block'], osd['serial'], osd['scsipath']))
        conn.commit()
        conn.close()

    def getcurrentdbrows(self):
        conn = sqlite3.connect(self.rootpath + 'osd.db')
        c = conn.cursor()
        c.execute('SELECT * FROM osd')
        pastcephmounpoints = [dict(zip([column[0] for column in c.description], row))
                              for row in c.fetchall()]
        conn.close()
        return pastcephmounpoints

    def getsummary(self):
        frow = []
        for row in sorted(self.getcurrentdbrows(), key=lambda x: x['osdid'], reverse=False):
            if any([True for x in self.getcephmountpoints() if
                    x['serial'] in row['serial'] and x['scsipath'] in row['scsipath']]):
                row['newblock'] = None
                frow.append(row)
            else:
                missing = list(filter(lambda x: x['scsipath'] == row['scsipath'], self.getdevices()))
                if len(missing) != 0:
                    missingdisk = missing[0]
                    missingdisk['osdid'] = row['osdid']
                    missingdisk['serial'] = {'old': row['serial'], 'new': missingdisk['serial']}
                    if any([True for x in self.getcephmountpoints() if x['scsipath'] in row['scsipath']]):
                        missingdisk['newblock'] = 'missing'

                    else:
                        missingdisk['newblock'] = list(filter(lambda x: (x.get('ID_SAS_PATH') if x.get('ID_SAS_PATH')
                                                                         else x.get('ID_PATH')) == row['scsipath'],
                                                              self.context.list_devices(subsystem='block',
                                                                                        DEVTYPE='disk')))[0]['DEVNAME']
                    frow.append(missingdisk)
                else:
                    row['newblock'] = 'missing'
                    frow.append(row)
        return frow


if __name__ == '__main__':
    obj = getpoints()
    for i in obj.getsummary():
        print(i)
