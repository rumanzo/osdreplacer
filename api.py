#!/usr/bin/env python2

import itertools
import json
import os
import re
import socket
import subprocess
import sys
import threading
import ConfigParser
from inspect import getframeinfo, currentframe
from netifaces import interfaces, ifaddresses, AF_INET
from shutil import copyfile


import urllib3
from flask import Flask, render_template, request, redirect

from osddb import getpoints

local_ip_addresses = set([x['addr'] for x in
                      itertools.chain(*filter(None, (ifaddresses(x).get(AF_INET) for x in interfaces())))])

sys.path.append(os.path.dirname(__name__))
app = Flask(__name__)

rootdir = '/opt/osdreplacer/'

config = '/etc/ceph/ceph.conf'

obj = getpoints.getpoints(rootpath=rootdir)

http = urllib3.PoolManager()

cfg= ConfigParser.ConfigParser()
cfg.readfp(open(rootdir + 'osdreplace.conf'))
cephhosts = cfg.get('cephhosts', 'hosts').split()
adminhosts = cfg.get('adminhosts', 'hosts').split()
adminhostsips = set([socket.gethostbyname(host) for host in adminhosts])

def cephexec(string):
    stdout, stderr = subprocess.Popen(re.split('\s+', string), stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE).communicate()
    print(stderr.decode('UTF-8'))
    return stderr.decode('UTF-8')


def getosdinfo(*hosts):
    answers = {}
    for host in hosts:
        try:
            answer = http.request('GET', 'http://{}/osdreplacer/osd/api/v1.0/scope'.format(host), timeout=2, retries=False)
        except Exception, e:
            print(e, host)
            continue
        if answer.data:
            if 'wait' in answer.data:
                return 'wait'
        else:
            continue
        answer = json.loads(answer.data)
        answers[answer[0]] = answer[1]
    return answers


def osdreplace(osdid, newblock, host):
    if os.path.exists(rootdir + 'osdcreate.lock'):
        return
    try:
        with open(rootdir + 'osdcreate.lock', 'w') as lock:
            lock.write('1')
        if os.path.exists('/var/lib/ceph/bootstrap-osd/ceph.keyring') and not os.path.exists(
                'ceph.bootstrap-osd.keyring'):
            copyfile('/var/lib/ceph/bootstrap-osd/ceph.keyring', 'ceph.bootstrap-osd.keyring')
        cephexec('ceph auth del osd.{} -f json -c {}'.format(osdid, config))
        cephexec('ceph osd rm {} -f json -c {}'.format(osdid, config))
        subprocess.Popen(['sgdisk', '-Z', newblock], stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE).communicate()
        cephexec('ceph-deploy --ceph-conf {} disk zap {}:{}'.format(config, host, newblock))
        stdout = cephexec('ceph-deploy --ceph-conf {} osd create {}:{}'.format(config, host, newblock))
        if re.search('.*Host \S+ is now ready for osd use.', stdout, re.MULTILINE):
            http.request('POST', 'http://{}/osdreplacer/osd/api/v1.0/scope'.format(host), fields={'osdid': osdid},
                             timeout=2, retries=False)
    finally:
        if os.path.exists(rootdir + 'osdcreate.lock'):
            os.remove(rootdir + 'osdcreate.lock')


def makeresponse(obj):
    return app.response_class(
        response=json.dumps(obj),
        status=200,
        mimetype='application/json'
    )



@app.route('/osd/api/v1.0/scope', methods=['GET', 'POST'])
@app.route('/osdreplacer/osd/api/v1.0/scope', methods=['GET', 'POST'])
def getsummary():
    obj = getpoints.getpoints(host=request.host, rootpath=rootdir)
    if request.method == 'GET':
        if os.path.exists(rootdir + 'osdcreate.lock'):
            return makeresponse(['wait'])
        obj.updatedb()
        return makeresponse([obj.hostname, obj.getsummary()])
    elif request.method == 'POST':
        osdid = request.form['osdid']
        obj.updatedb(updaterow=True, osdforclear=osdid)
        return "Successfully"
    else:
        return "Unsupported method"




@app.route('/replace', methods=['POST'])
@app.route('/osdreplacer/replace', methods=['POST'])
def replace():
    dt = {'url': request.form['url'], 'osdid': request.form['osdid'],
          'newblock': request.form['newblock'], 'host': request.form['host']}
    if local_ip_addresses & adminhostsips:  # intersection of ips
        if os.path.exists(rootdir + 'osdcreate.lock'):
            return redirect(request.scheme + '://' + dt['host'] + '/osdreplacer/')
        th = threading.Thread(target=osdreplace,
                              args=(dt['osdid'], dt['newblock'], dt['host'],))
        th.start()
        return render_template('countdown.html', url=dt['url'])
    else:
        for host in adminhosts:
            try:
                r = http.request('POST', 'http://{}/osdreplacer/replace'.format(host), fields=dt, timeout=2, retries=False)
                if r.status == 200:
                    return render_template('countdown.html', url=dt['url'])
            except:
                continue
        return render_template('countdown.html', url=dt['url'])


@app.route('/')
@app.route('/osdreplacer/')
def index():
    defaulturl = request.url
    hostsinfo = getosdinfo(*cephhosts)
    if hostsinfo == 'wait':
        return render_template('wait.html')
    return render_template('index.html', hostsinfo=hostsinfo, url=defaulturl)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
