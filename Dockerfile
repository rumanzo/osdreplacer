FROM centos:7.5.1804
MAINTAINER Alexey Kostin rumanzo@yandex.ru
RUN sed -i -e 's/mirrorlist=/#mirrorlist=/g' -e 's/#baseurl=http:\/\/mirror.centos.org\//baseurl=https\:\/\/mirror\.yandex\.ru\//g' /etc/yum.repos.d/CentOS*; \
    yum -y update; yum -y install epel-release; \
    sed -i -e 's/mirrorlist=/#mirrorlist=/g' -e 's/#baseurl=http\:\/\/download\.fedoraproject\.org\/pub\/epel\//baseurl=https\:\/\/mirror\.yandex\.ru\/epel\//g' /etc/yum.repos.d/epel*; \
    sed -i 's/.el7.centos/.el7/g' /etc/rpm/macros.dist; \
    yum -y install yum-utils rpm-build rsync; \
    yum clean all;\
    mkdir -p /root/rpmbuild/{SOURCES,SPECS}
WORKDIR /root/
CMD version=$(awk '/Version/ {print $2}' /root/osdreplacer/osdreplacer.spec); \
    name=osdreplacer\-${version}; \
    cp -a /root/osdreplacer /root/${name}; \
    chown -R root:root /root/${name}; \
    mv /root/${name}/osdreplacer.spec /root/rpmbuild/SPECS/osdreplacer.spec; \
    rm -rf /root/${name}/{.gitignore,Dockerfile,build.sh,.git}; \
    tar cfJ /root/rpmbuild/SOURCES/${name}.tar.xz -C /root/ ${name} ; \
    yum-builddep -y /root/rpmbuild/SPECS/osdreplacer.spec; \
    rpmbuild -ba /root/rpmbuild/SPECS/osdreplacer.spec; \
    find /root/rpmbuild/ -type f -iname '*.rpm' -exec mv {} /root/osdreplacer/ \;
