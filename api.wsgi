import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from api import app as application
application.debug = True

if __name__ == "__main__":
    application.run(host='127.0.0.1', port=8080, debug=True)
