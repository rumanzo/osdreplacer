#!/bin/bash
docker build -t osdreplacerbuild .
docker run --rm -h buildmachine.local --name osdreplacerbuild -v $(pwd):/root/osdreplacer/ osdreplacerbuild:latest
